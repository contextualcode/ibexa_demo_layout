// fractal.config.js

const liveServer = 'https://layoutless-test.s3.amazonaws.com';
const devServer = 'http://localhost:9000';
const baseTheme = 'base_theme';

const path = require('path');
const fse = require('fs-extra');
const fs = require('fs');
const twigTwig = require('twig');

const Translator = require('bazinga-translator');

fs.readFile('dist/translations/en.json', 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }
  Translator.fromJSON(data);
});

var fractal_class = require('@frctl/fractal');

const fractal = fractal_class.create();

fractal.set('project.title', 'Ibexa Demo');
fractal.components.set('path', __dirname + '/src/components');

const utils = require('@frctl/fractal').utils;

var trans_default_domains = {};

var Twig = require('twig');

const twigAdapter = require('@frctl/twig')({
    base: '/',
    namespaces: {
      'base_theme': './base_theme'
    },
    functions: {
        encore_entry_script_tags(tag_part) {
          let rawdata = fs.readFileSync('dist/entrypoints.json');
          let entrypoints = JSON.parse(rawdata);
          let output_entrypoints = entrypoints.entrypoints[tag_part]['js'];
          let outputstring = "";
          for (var tag_key in output_entrypoints) {
            outputstring += '<script src="' + output_entrypoints[tag_key] + '"></script>';
          }
          return outputstring;
        },
        encore_entry_link_tags: function(tag_part) {
          let rawdata = fs.readFileSync('dist/entrypoints.json');
          let entrypoints = JSON.parse(rawdata);
          let output_entrypoints = entrypoints.entrypoints[tag_part]['css'];
          let outputstring = "";
          for (var tag_key in output_entrypoints) {
            outputstring += '<link href="' + output_entrypoints[tag_key] + '" rel="stylesheet">';
          }
          return outputstring;
        },
        project_dir: function() {
          return '';
        },
        asset: function(path) {
          path = path.replace(/bundles\/[^\/]*/, '');
          path = path.replace(/\/img\//, '/images/');
          path = path.replace(/\/build\//, '/');
          path = path.replace(/build\//, '/');
          return path;
        },
        layoutlessAsset: function(path) {
          return devServer + path;
        },
        ez_render_field: function(v, k) {
          if (typeof v[k] == 'object') {
            root_dir = 'src/components/'+baseTheme+'/fieldtype/';
            scope_label = v[k].scope_label;
            template = scope_label + '.html.twig';
            data = {};
            //console.log({scope_label: this.context.content[scope_label]});
            data['content'] = this.context.content;
            data[scope_label] = this.context.content[scope_label];
            return Twig
            .twig({
              namespaces: {
              'base_theme': 'src/components/' + baseTheme
              },
              allowInlineIncludes: true,
              base: root_dir ,
              path: root_dir + template,
              async: false
            })
            .render(data);
          }
          return v[k];
        },
        constant: function(text) {
          constants = {'EzSystems\\\\EzPlatformCoreBundle\\\\EzPlatformCoreBundle::VERSION': '3.3.1'};
          return (constants[text] == undefined) ? text: constants[text];
        },
        url: function(text) {
          return '/admin';
        },
        ez_content_name: function(content) {
          return content.name;
        }
    },
    filters: {
        trans: function(id, params, domain, locale) {
          processParams = params[0] || {}

          // normalizes params (removes placeholder prefixes and suffixes)
          for (var key in processParams) {
            if (
              processParams.hasOwnProperty(key) &&
              key[0] == Translator.placeHolderPrefix &&
              key[key.length - 1] == Translator.placeHolderSuffix
            ) {
              processParams[key.substr(1, key.length - 2)] = processParams[key]
              delete processParams[key]
            }
          }

          return Translator.trans(id, processParams, domain, locale)
        },
        desc: function(k) {
          return k;
        }
    },
    tags: {
      trans_default_domain: function(Twig) {
          // usage: {% trans_default_domain "ajax" %}
          return {
              // unique name for tag type
              type: "trans_default_domain",
              // regex match for tag (flag white-space anything)
              regex: /^trans_default_domain\s+(.+)$/,
              // this is a standalone tag and doesn't require a following tag
              next: [ ],
              open: true,

              // runs on matched tokens when the template is loaded. (once per template)
              compile: function (token) {
                  var expression = token.match[1];

                  // Compile the expression. (turns the string into tokens)
                  token.stack = Twig.expression.compile.apply(this, [{
                      type:  Twig.expression.type.expression,
                      value: expression
                  }]).stack;

                  delete token.match;
                  return token;
              },

              // Runs when the template is rendered
              parse: function (token, context, chain) {
                  // parse the tokens into a value with the render context
                  var name = Twig.expression.parse.apply(this, [token.stack, context]),
                      output = '';

                      trans_default_domains[name] = true;

                  return {
                      chain: false,
                      output: output
                  };
              }
          };
      }
    }
});

fractal.components.engine(twigAdapter);
fractal.components.set('ext', '.html.twig');
fractal.web.set('static.path', __dirname + '/dist');

function exportTemplates(args, done) {

  const app = this.fractal;
  const items = app.components.flattenDeep().toArray();

  let date_ob = new Date();
  let date = ("0" + date_ob.getDate()).slice(-2);
  let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
  let year = date_ob.getFullYear();
  let hours = date_ob.getHours();
  let minutes = date_ob.getMinutes();
  const deployKey = year + month  + date + hours + minutes;

  fs.rmdir('build', { recursive: true }, (err) => {
    if (err) {
        throw err;
    } else {

      fse.copy('dist', 'build', function (err) {
        if (err) {
          console.error(err);
        } else {
        
          items.push({
            relViewPath: 'js/main.js',
            alias: 'main.sj',
            getContent: function() {return new Promise(function(resolve, reject){
              fs.readFile('src/assets/js/main.js', 'utf8', (err, data) => {
                  err ? reject(err) : resolve(data);
              });
            })}
          })
    
          items.push({
            relViewPath: 'js/twig.min.js',
            alias: 'twig.min.js',
            getContent: function() {return new Promise(function(resolve, reject){
              fs.readFile('src/assets/js/twig.min.js', 'utf8', (err, data) => {
                  err ? reject(err) : resolve(data);
              });
            })}
          })
        
          for (const item of items) {
    
            buildbase = (item.relViewPath.indexOf('.js') > 0) ? 'build' : 'build/templates/' + deployKey;
      
            const exportPath = path.join('./', buildbase, item.relViewPath);
    
            var job = item.getContent().then(str => {
              if (item.relViewPath.indexOf('main.js') > 0) {
                try {
                  EZVARtemplate_overrides = fs.readFileSync("src/config/overrides.json");
                  str=str.replace(/EZVARtemplate_overrides = {}/g, "EZVARtemplate_overrides = " + EZVARtemplate_overrides);
                } catch(err) {
                }

                try {
                  EZVARentrypoints = fs.readFileSync("dist/entrypoints.json");
                  str=str.replace(/EZVARentrypoints = {}/g, "EZVARentrypoints = " + EZVARentrypoints);
                } catch(err) {
                }

                try {
                  EZVARtranslations = fs.readFileSync("dist/translations/en.json");
                  str=str.replace(/EZVARtranslations = {}/g, "EZVARtranslations = " + EZVARtranslations);
                } catch(err) {
                }

                str=str.replace(/--baseTheme--/g, baseTheme);
                str=str.replace(/--deployKey--/g, deployKey);
                str=str.replace(/--assetServer--/g, (args.options.dev == undefined) ? liveServer : devServer);
              }
              fse.outputFile(exportPath, str, err => {
                if(err) {
                  console.log(err);
                }
              })
            })
          }
        }
      });

    }
  });
}

fractal.cli.command('build', exportTemplates,  {
  description: 'Export all component templates',
  options: [
    ['-d, --dev', 'Build in dev mode'],
  ]
});

// prevent default fractal build from firing
var cli = require('@frctl/fractal/src/cli');
const _ = require('lodash');
const requireAll = require('require-all');
class CliNew extends cli {
  exec() {
    _.forEach(requireAll({dirname : this._commandsDir, filter : function (fileName) {
      if (fileName == 'web.build.js') return;
      return fileName;
    } } ), (c) => this.command(c.command, c.action, c.config || {}));
    return arguments.length ? this._execFromString.apply(this, Array.from(arguments)) : this._execFromArgv();
  }
}
CliNew = new CliNew(fractal);
fractal.cli.exec = CliNew.exec;

module.exports = fractal; // export the configured Fractal instance for use by the CLI tool.

// build with yarn encore prod --config-name coco && fractal export
// deploy  with aws s3 sync build s3://layoutless-test --acl public-read --delete