const axios = require('axios');
const layoutless = require('layoutless');
var output = {};
const srcURL = 'https://ibexa.test.master-7rqtwti-ddcmi5twknlbq.us-4.platformsh.site/headless';

module.exports = new Promise((resolve, reject) => {
  axios.get(srcURL)
   .then(function (response) {
    output = layoutless.getVDOMFromHtml(response.data.replace(/\"\/var\/site/g, '"https://ibexa.test.master-7rqtwti-ddcmi5twknlbq.us-4.platformsh.site/var/site'));
    context = {context: output};
    resolve(context);
   })
   .catch(function (error) {
    resolve();
   });
 });