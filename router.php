<?php
header("Access-Control-Allow-Origin: *");
if (strpos($_SERVER["REQUEST_URI"], '.css') !== false) header("Content-type: text/css");
if (strpos($_SERVER["REQUEST_URI"], '.js') !== false) header("Content-type: text/javascript");
if (strpos($_SERVER["REQUEST_URI"], '.gif') !== false)header('Content-Type: image/gif');
if (strpos($_SERVER["REQUEST_URI"], '.jpg') !== false)header('Content-Type: image/jpeg');
if (strpos($_SERVER["REQUEST_URI"], '.png') !== false)header('Content-Type: image/png');
if (strpos($_SERVER["REQUEST_URI"], '.ico') !== false)header('Content-Type: image/x-icon');
if (file_exists('build'.$_SERVER['REQUEST_URI'])) {
    readfile('build'.$_SERVER['REQUEST_URI']);
} else {
    header("404 Not Found", true, 404);
}